﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonControll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void LbuttonDown()
    {
        transform.Translate(0, -3, 0);
    }

    public void RButtonDown()
    {
        transform.Translate(0, 3, 0);
    }
}
