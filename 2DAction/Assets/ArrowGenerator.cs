﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowGenerator : MonoBehaviour
{
    public GameObject arrowPrefab;
    float span = 1.0f;
    float delta = 0;

    

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;//ゲームの時間
        Debug.Log(delta);
        if (delta > span)
        {
            delta = 0;

            GameObject go = Instantiate(arrowPrefab) as GameObject;
            int px = Random.Range(-6, 7);
            go.transform.position = new Vector3(7, px, 0);
        }
    }
}
