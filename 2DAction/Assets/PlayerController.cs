﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, 0.07f, 0);
        }

        if(Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, -0.07f, 0);
        }

    }

    
    public void LButtonDown()
    {
        transform.Translate(0, 3, 0);
    }

    public void RButtonDown()
    {
        transform.Translate(0, -3, 0);
    }

}
