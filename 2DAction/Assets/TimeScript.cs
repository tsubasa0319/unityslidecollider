﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScript : MonoBehaviour
{　　　//カウントダウン
    public float countdown = 30f;
    //時間を表示する変数
    public Text timeText;
    //ポーズしているかどうか
    private bool isPose = false;

    // Update is called once per frame
    void Update()
    {　　　//クリックされたとき
        if (Input.GetMouseButtonDown(0))
        {
            //ポーズ中にクリックされたとき
            if (isPose)
            {
                //ポーズを解除
                isPose = false;
            }
            //進行中にクリックされたとき
            else
            {//ポーズ状態にする
                isPose = true;
            }
        }
        //ポーズしてるかどうか
        if (isPose)
        {
            //ポーズ中であることを表示
            timeText.text = "ポーズ中";
            //カウントダウンしない
            return;
        }
        //時間カウント
        countdown -= Time.deltaTime;
        //時間表示
        timeText.text = countdown.ToString("f1") + "秒";
        //カウントダウンが０になったら
        if(countdown<=0)
        {
            timeText.text = "終了";
        }
    }
}
