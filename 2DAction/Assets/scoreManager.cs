﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreManager : MonoBehaviour
{


    public GameObject score_object = null;
    public int score_num = 0;



    // Update is called once per frame
    void Update()
    {


        Text score_text = score_object.GetComponent<Text>();

        score_text.text = "score;" + score_num;

        score_num += 1;

    }
}

